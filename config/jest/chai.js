import chai from 'chai';

export const installChai = () => {
    global.assert = chai.assert;
    global.expect = chai.expect;
    global.should = chai.should();

    return chai;
};
