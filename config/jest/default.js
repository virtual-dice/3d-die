import { installChai } from './chai';
import { installChaiAsPromised } from './chai-as-promissed';
import { installSinon } from './sinon';

const chai = installChai();
installSinon(chai);
installChaiAsPromised(chai);
