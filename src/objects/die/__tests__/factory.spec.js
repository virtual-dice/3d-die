import { expect } from 'chai';

describe('Given the createDie factory', () => {
    let createDie;

    describe('When imported as a Node module', () => {
        beforeEach(() => {
            ({ createDie } = require('../factory'));
        });

        it('Then it should be a function', () => {
            createDie.should.be.a('function');
        });

        describe('And executed', () => {
            let actualController;

            beforeEach(() => {
                actualController = createDie();
            });

            it('Then it should be null', () => {
                expect(actualController).to.be.undefined;
            });
        });
    });
});
