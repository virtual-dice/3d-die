module.exports = {
    collectCoverageFrom: [
        'src/**/*.js',
        '!**/index.js'
    ],
    coverageDirectory: '<rootDir>/coverage/',
    setupFiles: [
        '<rootDir>/config/jest/default.js'
    ],
    testMatch: [
        '**/__tests__/**/*.spec.js'
    ],
    transform: {
        '^.+\\.js?$': 'babel-jest'
    },
    watchPlugins: ['jest-watch-master']
};
