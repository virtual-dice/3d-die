import sinonChai from 'sinon-chai';

export const installSinon = chai => {
    chai.use(sinonChai);
};
